package com.hello.business;

// import com.ch.logger.utils.ChLog;
// import com.ch.logger.utils.LogbackUtils;
import com.hello.common.ApplicationConfigs;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Lazy;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.sql.DataSource;
import java.sql.*;
import java.util.Date;

/**
 * @author: mandala
 * date：2019/10/24 15:25
 */
@RestController
public class HelloController {

    @Autowired
    ApplicationConfigs applicationConfigs;

    HikariDataSource hikariDataSource;


    static final Logger log = LoggerFactory.getLogger(HelloController.class);

    @RequestMapping(path = "hello")
    public String get(String data) {

        String c1 = applicationConfigs.getC1();
        String c2 = applicationConfigs.getC2();
        String c3 = applicationConfigs.getC3();
        String c4 = applicationConfigs.getC4();
        String c5 = applicationConfigs.getC5();
        System.out.println("111");
        System.out.println("111");
        System.out.println("http111://ch.com");
        System.out.println("https://api.geetest.com");


        System.out.println("c1: " + c1);
        System.out.println("c2: " + c2);
        System.out.println("c3: " + c3);
        System.out.println("c4: " + c4);
        System.out.println("c5: " + c5);

        return "时间: " + (new Date());
    }


    /**
     * 重庆QC数据库状态查询
     *
     * @return java.lang.String 查询结果
     * @Author zhangke
     * @Date 14:48 2021/9/15
     **/
    @GetMapping("/db-status")
    public String dbStatus() throws SQLException, JSONException {
        try (Connection conn = dataSource().getConnection(); Statement stmt = conn.createStatement();) {
            String SQL = "select * from  td.BUG where (BG_BUG_ID=1);";
            ResultSet rs = stmt.executeQuery(SQL);
            JSONArray json = new JSONArray();
            StringBuffer sb = new StringBuffer();
            ResultSetMetaData rsmd = rs.getMetaData();
            JSONObject obj = new JSONObject();
            while (rs.next()) {
                int numColumns = rsmd.getColumnCount();
                for (int i = 1; i < numColumns + 1; i++) {
                    String column_name = rsmd.getColumnName(i);

                    if (rsmd.getColumnType(i) == java.sql.Types.ARRAY) {
                        obj.put(column_name, rs.getArray(column_name));
                    } else if (rsmd.getColumnType(i) == java.sql.Types.BIGINT) {
                        obj.put(column_name, rs.getInt(column_name));
                    } else if (rsmd.getColumnType(i) == java.sql.Types.BOOLEAN) {
                        obj.put(column_name, rs.getBoolean(column_name));
                    } else if (rsmd.getColumnType(i) == java.sql.Types.BLOB) {
                        obj.put(column_name, rs.getBlob(column_name));
                    } else if (rsmd.getColumnType(i) == java.sql.Types.DOUBLE) {
                        obj.put(column_name, rs.getDouble(column_name));
                    } else if (rsmd.getColumnType(i) == java.sql.Types.FLOAT) {
                        obj.put(column_name, rs.getFloat(column_name));
                    } else if (rsmd.getColumnType(i) == java.sql.Types.INTEGER) {
                        obj.put(column_name, rs.getInt(column_name));
                    } else if (rsmd.getColumnType(i) == java.sql.Types.NVARCHAR) {
                        obj.put(column_name, rs.getNString(column_name));
                    } else if (rsmd.getColumnType(i) == java.sql.Types.VARCHAR) {
                        obj.put(column_name, rs.getString(column_name));
                    } else if (rsmd.getColumnType(i) == java.sql.Types.TINYINT) {
                        obj.put(column_name, rs.getInt(column_name));
                    } else if (rsmd.getColumnType(i) == java.sql.Types.SMALLINT) {
                        obj.put(column_name, rs.getInt(column_name));
                    } else if (rsmd.getColumnType(i) == java.sql.Types.DATE) {
                        obj.put(column_name, rs.getDate(column_name));
                    } else if (rsmd.getColumnType(i) == java.sql.Types.TIMESTAMP) {
                        obj.put(column_name, rs.getTimestamp(column_name));
                    } else {
                        obj.put(column_name, rs.getObject(column_name));
                    }
                }
                json.put(obj);
            }
            return json.toString();
        } catch (
                Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    /**
     * 手动初始化数据库连接
     * @Author zhangke
     * @Date 13:18 2021/9/29
     * @return javax.sql.DataSource
     **/
    public DataSource dataSource() {
        if (hikariDataSource != null) {
            return hikariDataSource;
        }
        HikariConfig jdbcConfig = new HikariConfig();
        jdbcConfig.setPoolName(getClass().getName());
        jdbcConfig.setDriverClassName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        jdbcConfig.setJdbcUrl("jdbc:sqlserver://10.131.208.12:1433;DatabaseName=aviation_foc_db;user=ecreader;password=ecreader");
        jdbcConfig.setUsername("ecreader");
        jdbcConfig.setPassword("ecreader");
        jdbcConfig.setMaximumPoolSize(10);
        jdbcConfig.setMaxLifetime(6000);
        jdbcConfig.setConnectionTimeout(10000);
        jdbcConfig.setIdleTimeout(2000);
        return new HikariDataSource(jdbcConfig);
    }

    @RequestMapping(path = "/logback")
    public String logbackDemo(Integer sendMode){

        log.info("记录日志: " + (new Date()));

        if(sendMode == 0){
            // LogbackUtils.calcNormal();
            // LogbackUtils.logbackStatus();

            // ChLog.info("Auto Hello 正常日志 时间标记"+(new Date()).toString());

        }else if(sendMode == 1){
            // LogbackUtils.calcException();
            // LogbackUtils.logbackStatus();

            // ChLog.info("Auto Hello 异常日志 时间标记"+(new Date()).toString());
        }else{
            // LogbackUtils.logbackStatus();
        }

        /**
         *
         *  1、异常计数 模式切换到 异常
         *  2、正常计数 模式恢复到 正常
         *  3、异常计数 时间重置
         *  4、正常计数 时间重置
         *
         * */

        return "11223344";
    }

}
