package com.hello;

// import com.ch.logger.utils.LogbackUtils;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
public class AutoHelloApplication {

	public static void main(String[] args) {
		SpringApplication.run(AutoHelloApplication.class, args);
		// LogbackUtils.initLogMode();
	}
}
