package com.hello.common;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties(prefix = "ecommerce.autohello")
public class ApplicationConfigs {

    private String c1;
    private String c2;
    private String c3;
    private String c4;
    private String c5;
}

